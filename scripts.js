$(document).ready( () =>{
   const navBar = document.querySelector('.servicesNav');
   const listElem = document.querySelectorAll('.servicesInfoElem');
   const triangles = document.querySelectorAll('.triangle');
   listElem.forEach(item => {
       item.classList.remove('servicesInfoElem');
       item.classList.add('displayNone')
   });
   triangles.forEach(item => {item.classList.add('displayNone')});
   navBar.addEventListener('click', function (event) {
       listElem.forEach(item => { item.classList.remove('servicesInfoElem');
           item.classList.add('displayNone')});
       for (let item of navBar.children){
           if (item.classList.contains('navElemHover')) {
               item.classList.remove('navElemHover');
               item.lastElementChild.classList.add('displayNone')
           }
       }
       const target = event.target;
       let position;
       if (target.tagName === 'SPAN'){
           target.parentElement.classList.add('navElemHover');
           target.parentElement.lastElementChild.classList.remove('displayNone');
           position = target.parentElement.innerText;
       }else {
           target.classList.add('navElemHover');
           target.lastElementChild.classList.remove('displayNone');
           position = target.innerText;
       }
       listElem.forEach(item => {
           if (item.dataset.info === position.toString()){
               item.classList.add('servicesInfoElem');
               item.classList.remove('displayNone')
           }
       })
   });
    navBar.children[0].click();
    function addDisplayNone(elems, position) {
        elems.each(function (index, element) {
            if (index > position) {
                element.classList.add('displayNone');
            }
        })
    }
    function removeDisplayNone(elems, position) {
        elems.each(function (index, element) {
            if (index > position) element.classList.remove('displayNone');
        })
    }
    const graphicDesignPhotos = $('.graphicDesign');
    const webesignphotos = $('.webDesign');
    const landingPagePhotos = $('.landingPage');
    const wordPressphotos = $('.wordPress');
    addDisplayNone(graphicDesignPhotos, 2);
    addDisplayNone(webesignphotos, 2);
    addDisplayNone(landingPagePhotos, 2);
    addDisplayNone(wordPressphotos,2);
    const allWorkImages = $('.imagePhoto > img');
    const workImage = $('.imagePhoto');
    const worksNav = $('.worksNav');
    worksNav.children()[0].classList.add('worksNavElemActive');
    worksNav.on('click', function (event) {
        worksNav.children().each( function (index, elem) {
            if (elem.classList.contains('worksNavElemActive')) elem.classList.remove('worksNavElemActive');
        });
        event.target.classList.add('worksNavElemActive');
        const aimFilter = event.target.dataset.info;
        if (aimFilter === 'All'){
            allWorkImages.each(function (index, elem) {
                if (elem.classList.contains('displayNone') && !elem.hasAttribute('data-info')) {
                    $(this).fadeIn('slow');
                    elem.classList.remove('displayNone');
                }
            });
            return;
        }
        allWorkImages.each(function (index, elem) {
            if (elem.classList.contains(aimFilter) && elem.classList.contains('displayNone') && !elem.hasAttribute('data-info')){
                elem.classList.remove('displayNone');
                $(this).fadeIn('slow');

            }
            if (elem.classList.contains(aimFilter) || elem.classList.contains('displayNone')) return 0;
            $(this).fadeOut('slow');
            elem.classList.add('displayNone');
            elem.style.cssText = ' ';
        })
    });

    const worksLoadMOreButton = $('.amazingWorks > .loadMoreButton');
    worksLoadMOreButton.on('click', function (event) {
        $('.load').each(function (index, elem) {
            $(this).fadeIn('slow');
        });
        setTimeout(function(){
            $('.load').each(function (index, elem) {
                $(this).fadeOut('slow');
            });
            allWorkImages.each(function (index, item) {
                if (!worksLoadMOreButton.attr('check') && index > 23) return;
                if (item.classList.contains('displayNone')) {
                    $(this).fadeIn('slow');
                    $(this).removeAttr('data-info');
                }
            });
            worksNav.children()[0].click();
            if (worksLoadMOreButton.attr('check')) {
                worksLoadMOreButton.fadeOut();
                return;
            }
            worksLoadMOreButton.attr('check', 'true');
        }, 3000)
    });

    $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav'
        });
        $('.slider-nav').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            dots: false,
            centerMode: true,
            focusOnSelect: true
        });

    const parameters = {
        duration: 500,
        progress: function (animation, progress) {
            $('#progress')
                .width(parseInt(progress * 100) + '%')
                .text(parseInt(progress * 100) + '%');
        },
        complete: function () {
            $('#progress')
                .css('width', '0%')
                .text('0%');
        }
    };

    const section = $('.workSectionName');

    section.hide();

    workImage.on('mouseenter', function (event) {
        if (event.target.tagName !== 'IMG' ) return 0;
        $(this).children().first().slideDown(parameters);
        event.stopPropagation();
    });
    workImage.on('mouseleave', function (event) {
        $(this).children().first().slideUp(parameters);
    });

    $('.grid').masonry({
        itemSelector: '.grid-item',
        columnWidth: '.grid-sizer',
        percentPosition: true
    });

    $('.load').each(function () {
        $(this).fadeOut();
    })




});